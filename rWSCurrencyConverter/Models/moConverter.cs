﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rWSCurrencyConverter.Models
{
    public class moConverter
    {
        public moCurrency obBase { get; set; }
        public DateTime dDate { get; set; }
        public moCurrency obRate { get; set; }
        public double nuValue { get; set; }

    }

    public class moCurrency
    {
        public string vcIdCurrency { get; set; }
        public string vcCurrency { get; set; }
        public string vcSymbol { get; set; }

    }
}