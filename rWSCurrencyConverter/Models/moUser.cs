﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace rWSCurrencyConverter.Models
{
    public class moUser
    {
        public int iIdUser { get; set; }
        public string vcUser { get; set; }
        public string vcPassword { get; set; }
        public string vcName { get; set; }
        public string vcLastName { get; set; }

    }
}