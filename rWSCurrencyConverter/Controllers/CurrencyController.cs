﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using rWSCurrencyConverter.Models;

namespace rWSCurrencyConverter.Controllers
{
    public class CurrencyController : ApiController
    {
        moCurrency[] objData = new moCurrency[]{
        new moCurrency{vcIdCurrency="USD",vcCurrency="US Dollar",vcSymbol="$"},
        new moCurrency{vcIdCurrency="EUR",vcCurrency="Euro",vcSymbol="€"},
        new moCurrency{vcIdCurrency="PEN",vcCurrency="Nuevo Sol",vcSymbol="S/."},
        new moCurrency{vcIdCurrency="COP",vcCurrency="Colombian Peso",vcSymbol="$"},
        };

        public IEnumerable<moCurrency> GetCurrency()
        {
            return objData;
        }
    }
}
