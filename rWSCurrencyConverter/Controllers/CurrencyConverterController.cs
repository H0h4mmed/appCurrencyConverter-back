﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using rWSCurrencyConverter.Models;

namespace rWSCurrencyConverter.Controllers
{
    public class CurrencyConverterController : ApiController
    {
        //Se define una matriz para el caso de ejemplo
        moConverter[] objData = new moConverter[]{
        new moConverter{obBase=new moCurrency {vcIdCurrency="USD",vcCurrency="Dollar"},dDate=DateTime.Now,obRate=new moCurrency {vcIdCurrency="EUR",vcCurrency="Euro"},nuValue=0.89712},
        new moConverter{obBase=new moCurrency {vcIdCurrency="USD",vcCurrency="Dollar"},dDate=DateTime.Now,obRate=new moCurrency {vcIdCurrency="PEN",vcCurrency="Sol"},nuValue=3.32324},
        new moConverter{obBase=new moCurrency {vcIdCurrency="EUR",vcCurrency="Dollar"},dDate=DateTime.Now,obRate=new moCurrency {vcIdCurrency="PEN",vcCurrency="Sol"},nuValue=4.32324},
        };

        public IEnumerable<moConverter> GetRates()
        {
            return objData;
        }

        public IHttpActionResult GetRate([FromUri] string vcBase, [FromUri] string vcSymbol)
        {
            //List<moExchange> Exchange = null;
            var objConverter = objData.FirstOrDefault((e) => e.obBase.vcIdCurrency == vcBase && e.obRate.vcIdCurrency == vcSymbol);

            /*Exchange.Add(new moExchange()
            {
                vcBase = vcBase,
                dDate = DateTime.Now,
                
            });
            */
            try
            {
                if (objConverter != null)
                {
                    return Ok(objConverter);

                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception exception)
            {
                return NotFound();
            }
        }

    }
}
