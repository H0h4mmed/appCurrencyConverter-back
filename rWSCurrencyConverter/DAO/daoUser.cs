﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Linq;
using rWSCurrencyConverter.Models;

namespace rWSCurrencyConverter.DAO
{
    public class daoUser
    {
        public bool ValidateUser(moUser User)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ToString());
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "global.sp_login";
                cmd.Parameters.AddWithValue("@vcUser", User.vcUser);
                cmd.Parameters.AddWithValue("@vcPassword", User.vcPassword);
                con.Open();
                if (Convert.ToInt32(cmd.ExecuteScalar()) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                con.Close();
            }
        }

        
    }
}