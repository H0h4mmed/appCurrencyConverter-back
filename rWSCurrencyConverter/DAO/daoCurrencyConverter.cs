﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using rWSCurrencyConverter.Models;

namespace rWSCurrencyConverter.DAO
{
    public class daoCurrencyConverter
    {
        public moConverter fGetRate(string vcBase, string vcSymbol)
        {
            moConverter objConverter = new moConverter();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["sSqlConnection"].ToString());
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "converter.sp_sel_rate";
                cmd.CommandTimeout =1200;
                cmd.Parameters.AddWithValue("@vcBase", vcBase);
                cmd.Parameters.AddWithValue("@vcSymbol", vcSymbol);
               
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    objConverter.obBase.vcIdCurrency = rdr["vcIdInputCurrency"].ToString();
                    objConverter.obBase.vcCurrency = rdr["vcInputCurrencyBase"].ToString();
                    objConverter.dDate = Convert.ToDateTime(rdr["dDate"]);
                    objConverter.nuValue = Convert.ToDouble(rdr["nuValue"]);
                    objConverter.obRate.vcIdCurrency = rdr["vcOutputIdCurrency"].ToString();
                    objConverter.obRate.vcCurrency = rdr["vcOutputCurrency"].ToString();

                }

                return objConverter;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                con.Close();
            }
        }
    }
}